#! /usr/bin/env python3

import time
import random
import logging
import traceback
from pathlib import Path

import pyautogui


# # # Globals / Logger # # #

# Logger Setup
try:
    Cwd = Path(".").resolve()
    # LogDir = Cwd / "data/logs"
    LogFile = Cwd / "retry_upload.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    logger = logging.getLogger(__name__)  # Get logger with module name
    fileHndl = logging.FileHandler(LogFile)  # Get file handler
    streamHndl = logging.StreamHandler()  # Get stream handler
    formatter = logging.Formatter(LogFmt)  # Get formatter

    # Handle message levels
    logger.setLevel(logging.DEBUG)
    fileHndl.setLevel(logging.DEBUG)
    streamHndl.setLevel(logging.DEBUG)

    # Set formatter
    fileHndl.setFormatter(formatter)
    streamHndl.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(fileHndl)
    logger.addHandler(streamHndl)

except RuntimeWarning:
    # Attempt to log setup failure
    print(traceback.format_exc())
    print("An exception occured... Logging disabled.")
    # Notify setup failure outside logging
    logging.disable()  # Disable logging after failed setup


def main():
    while True:
        retry_onscreen = pyautogui.locateOnScreen("retry.png")
        replace_onscreen = pyautogui.locateOnScreen("replace.png")
        if retry_onscreen is not None:
            logger.debug("Found retry button.")
            coordinates = pyautogui.center(retry_onscreen)
            pyautogui.click(coordinates)
            pyautogui.moveRel(150, 200)  # move mouse off where button was
        if replace_onscreen is not None:
            logger.debug("Found replace button.")
            coordinates = pyautogui.center(replace_onscreen)
            pyautogui.click(coordinates)
            pyautogui.moveRel(150, 200)  # move mouse off where button was
        time.sleep(10)  # wait 10 seconds between checks
        logger.debug("Pressing esc...")
        pyautogui.press("esc")  # keep the screen from locking


if __name__ == "__main__":
    try:
        main()
    except Exception:
        logger.critical(traceback.format_exc())
