#!/usr/bin/env python3

"""A script for joining multiple CSV files."""
"""TMP: Joins all CSV files into 1."""

import csv
import itertools
from pathlib import Path

from modules import helpers


def merge_lists(List1: list, List2: list) -> list:
    """Takes a list of multi-dimensional lists and merges the
    corresponding values to produce a single multi-dimensional list."""
    return [list(itertools.chain(*entry))
                   for entry in zip(List1, List2)]


def deep_merge_lists(Lists: list) -> list:
    """Calls 'merge_lists' multiple times to merge more than two
    lists."""
    deep_merged_list = list()
    for l in Lists:
        deep_merged_list = merge_lists(deep_merged_list, l)
    return deep_merged_list


def csv_joiner() -> None:
    Cwd = Path(".").resolve()  # Get current working directory

    # Loop Lists
    csv_files = list()
    csv_lists = list()

    # Get a list of all csv files in Cwd
    for file in Cwd.iterdir():
        if file.suffix.lower() == ".csv":
            csv_files.append(file)

    # Open each file and extract the values as lists
    # Use filename order or a specified order
    for file in csv_files:
        csv_lists.append(helpers.load_csv(file))

    # BUG HERE
    input(f'list1: {csv_lists[0]}')
    input(f'list2: {csv_lists[1]}')
    input(f"merge_lists check: {merge_lists(csv_lists[0], csv_lists[1])}")

    # Combine the list of lists into a flat list
    joined_csv = deep_merge_lists(csv_lists)

    input(f'Joined = {joined_csv}')
    for i in range(20):
        print("")
    # Known End: BUG HERE


    helpers.to_csv(joined_csv, Cwd / "merged.csv")  # Write to csv

    return None


def main() -> None:
    csv_joiner()


if __name__ == "__main__":
    main()
