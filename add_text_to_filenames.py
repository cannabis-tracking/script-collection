#! /usr/bin/env python3

from datetime import datetime
from pathlib import Path


def main():
    Input_Message = "Drag the folder that contains the files to rename: "
    Input_Message2 = ("Enter the date or text you want to add in front \n"
                      "of each filename: ")
    
    rename_folder = input(Input_Message)
    rename_folder = rename_folder.strip("'\" ")

    renamedir = Path(rename_folder).expanduser().absolute()

    if not renamedir.exists() or not renamedir.is_dir():
        input(f"Error, {str(renamedir)} does not exist or is not a folder...")
        exit(-1)

    # Get String to Add to the Beginning
    addtext = input(Input_Message2)
    
    for rename_file in renamedir.iterdir():
        oldname = rename_file.name
        newname = f"{addtext} {oldname}"
        rename_file.replace(newname)
    
    input("Renaming complete. Press enter to close this window...")

if __name__ == "__main__":
    main()
